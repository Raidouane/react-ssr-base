import React, { Component } from 'react';
import createHistory from 'history/createBrowserHistory';
import { BrowserRouter as Router } from 'react-router-dom';
import { ConnectedRouter, routerReducer } from 'react-router-redux';
import { loadComponents } from 'loadable-components';
import { render } from 'react-dom';

import App from '../shared/app';
import configureStore from '../shared/redux/store';
import sagas from '../shared/redux/sagas';
import withStyleAndStore from '../shared/app/withStyleAndStore';

// Grab the state from a global variable injected into the server-generated HTML
const preloadedState = window.__PRELOADED_STATE__;

const reducers = [{ routing: routerReducer }];

const history = createHistory();
// Allow the passed state to be garbage-collected
delete window.__PRELOADED_STATE__;

const store = configureStore(reducers, preloadedState, history);

// then run the saga
store.runSagas(sagas);

class Main extends Component {
    // Remove the server-side injected CSS.
    componentDidMount() {
        const jssStyles = document.getElementById('jss-server-side');
        if (jssStyles && jssStyles.parentNode) {
            jssStyles.parentNode.removeChild(jssStyles);
        }
    }

    render = () => (
        <Router>
            <App {...this.props} />
        </Router>
    );
}

loadComponents().then(() => {
    const AppWithRouter = (
        <ConnectedRouter history={history}>
            <Main />
        </ConnectedRouter >
    );
    const { AppWithStyleAndStore } = withStyleAndStore(AppWithRouter, store);

    render(
        AppWithStyleAndStore,
        document.getElementById('root'),
    );
});
