//@flow
import React from 'react';
import { renderRoutes } from 'react-router-config';
import { Switch } from 'react-router-dom';
import Helmet from 'react-helmet';

import AppContent from './styles';
import Routes from './routes';
import Menu from '../components/Menu';

const App = () => (
    <AppContent>
        <Helmet
            htmlAttributes={{ lang: 'fr', amp: undefined }} // amp takes no value
            titleTemplate="%s | React Base "
            titleAttributes={{ itemprop: 'name', lang: 'en' }}
            meta={[
                { name: 'description', content: 'SSR base' },
                { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            ]}
        />
        <Menu />
        <Switch>
            {renderRoutes(Routes)}
        </Switch>
    </AppContent>
);
export default App;
