import styled from 'react-emotion';

const AppContent = styled('div')({
    height: '100%',
});

export default AppContent;
