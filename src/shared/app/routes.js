//@flow
import loadable from 'loadable-components';

export const HomeView = loadable(() => import('../views/Home'));
export const CatsView = loadable(() => import('../views/Cats'));
const NotFound = loadable(() => import('../components/NotFound'));

const Routes = [
    {
        path: '/',
        exact: true,
        component: HomeView,
    },
    {
        path: '/cats',
        component: CatsView,
    },
    {
        component: NotFound,
    },
];

export default Routes;
