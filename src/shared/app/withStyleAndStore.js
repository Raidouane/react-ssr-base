import JssProvider from 'react-jss/lib/JssProvider';
import React from 'react';
import { Provider } from 'react-redux';
import { SheetsRegistry } from 'jss';
import { blueGrey } from '@material-ui/core/colors';
import { createGenerateClassName, createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

const createStyleManager = () => createMuiTheme({
    palette: {
        type: 'light',
        primary: blueGrey,
    },
    typography: {
        useNextVariants: true,
    },
});

const withStyleAndStore = (WrappedComponent, store) => {
    const theme = createStyleManager();
    const sheetsRegistry = new SheetsRegistry();
    const generateClassName = createGenerateClassName();

    return ({
        AppWithStyleAndStore: (
            <JssProvider registry={sheetsRegistry} generateClassName={generateClassName}>
                <MuiThemeProvider theme={theme}>
                    <Provider store={store}>
                        {WrappedComponent}
                    </Provider>
                </MuiThemeProvider>
            </JssProvider>
        ),
        sheetsRegistry,
    });
};

export default withStyleAndStore;
