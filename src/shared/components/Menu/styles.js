import { Link } from 'react-router-dom';
import styled from 'react-emotion';

export const NavBar = styled('nav')({
    display: 'block',
    position: 'absolute',
    right: '1rem',
});

export const NavLink = styled(Link)({
    margin: '1rem',
    paddingRight: '2rem',
    textDecoration: 'none',
    color: 'white',
    fontSize: '2rem',
});
