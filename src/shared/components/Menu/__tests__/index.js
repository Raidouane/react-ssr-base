import React from 'react';
import renderer from 'react-test-renderer';
import { StaticRouter } from 'react-router-dom';

import Menu from '../';

it('renders correctly', () => {
    const componentToTest = (
        <StaticRouter location="someLocation" context={{}}>
            <Menu />
        </StaticRouter>
    );
    const tree = renderer
        .create(componentToTest)
        .toJSON();

    expect(tree).toMatchSnapshot();
});
