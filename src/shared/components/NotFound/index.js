import React from 'react';
import Helmet from 'react-helmet';

import { Content, ResponsiveImage } from './styles';

import BadRequestImage from '../../Images/bad-request.svg';

const Index = () => (
    <Content>
        <Helmet title="Oops you're lost" />
        <ResponsiveImage src={BadRequestImage} alt="not found" />
    </Content>
);

export default Index;
