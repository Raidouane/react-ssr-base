import styled from 'react-emotion';

export const Content = styled('div')({
    display: 'flex',
    backgroundColor: '#df919d',
    height: '94%',
    justifyContent: 'center',
});

export const ResponsiveImage = styled('img')({
    width: '100%',
    height: 'auto',
});
