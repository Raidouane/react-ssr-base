import catsReducer from './cats/reducers';

const rootReducer = {
    cats: catsReducer,
};

export default rootReducer;
