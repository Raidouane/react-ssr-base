import { combineReducers, createStore, compose, applyMiddleware } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import createSagaMiddleware, { END } from 'redux-saga';
import {
    entitiesReducer,
    queriesReducer,
    queryMiddleware,
    mutateAsync,
    requestAsync,
    cancelQuery,
    getQueryKey,
} from 'redux-query';
import rootReducers from './reducers';

const sagaMiddleware = createSagaMiddleware();

const getQueries = ({ queries }) => queries;
const getEntities = ({ entities }) => entities;

const configureStore = (reducers = {}, state, historyMiddleware) => {
    const store = createStore(
        combineReducers({
            ...rootReducers,
            ...reducers,
            entities: entitiesReducer,
            queries: queriesReducer,
        }),
        state,
        compose(
            applyMiddleware(routerMiddleware(historyMiddleware), queryMiddleware(getQueries, getEntities), sagaMiddleware),
            typeof window !== 'undefined' && window.devToolsExtension ? window.devToolsExtension() : f => f,
        ),
    );

    store.runSagas = sagaMiddleware.run;

    store.close = () => store.dispatch(END);

    return store;
};

export default configureStore;
