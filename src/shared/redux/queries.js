import { map } from 'ramda';

export const getCats = () => ({
    url: 'https://api.thecatapi.com/v1/images/search',
    body: {
        format: 'json',
        limit: 5,
    },
    options: {
        method: 'GET',
        headers: {
            'x-api-key': '17d94b92-754f-46eb-99a0-65be65b5d18f',
            'Content-Type': 'application/json',
        },
    },
    update: {
        cats: (prev, next) => next,
    },
    transform: (cats: ?Object) => ({
        cats: map(({ url }) => url, cats),
    }),
});

export const changeNameMutation = name => ({
    url: '/api/change-name',
    body: {
        name,
    },
    update: {
        name: (prev, next) => next,
    },
});
