export const FETCH_CATS_REQUESTED = 'FETCH_CATS_REQUESTED';
export const FETCH_CATS_SUCCEEDED = 'FETCH_CATS_SUCCEEDED';
export const FETCH_CATS_FAILED = 'FETCH_CATS_FAILED';

export const fetchCats = () => ({
    type: FETCH_CATS_REQUESTED,
    payload: {},
});

export const receiveCats = payload => ({
    type: FETCH_CATS_SUCCEEDED,
    payload,
});
