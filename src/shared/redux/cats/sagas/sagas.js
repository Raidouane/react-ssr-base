import { put } from 'redux-saga/effects';
import { mutateAsync } from 'redux-query';

import {
    FETCH_CATS_SUCCEEDED,
    FETCH_CATS_FAILED,
} from '../actions';
import { getCats } from '../../queries';

export function* fetchCats() {
    const query = getCats();

    try {
        const { status } = yield put.resolve(mutateAsync(query));
        if (status < 200 || status >= 300) {
            yield put({
                type: FETCH_CATS_FAILED,
                error: {
                    status,
                    message: 'Il y a eu erreur à la récupération des chats',
                },
            });
            return;
        }

        yield put({
            type: FETCH_CATS_SUCCEEDED,
            hasRetrieveOnly: false,
        });
    } catch (error) {
        yield put({
            type: FETCH_CATS_FAILED,
            error,
        });
    }
}
