import { takeEvery } from 'redux-saga/effects';

import { FETCH_CATS_REQUESTED } from '../actions';
import { fetchCats } from './sagas';

export default function* fetchCatsSaga() {
    yield takeEvery(FETCH_CATS_REQUESTED, fetchCats);
}
