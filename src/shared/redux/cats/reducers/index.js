import { FETCH_CATS_SUCCEEDED, FETCH_CATS_REQUESTED, FETCH_CATS_FAILED } from '../actions';
import { failure, success } from '../../resultStates';

const initialState = [];

const catsReducers = (previousState = initialState, { type, error }) => {
    switch (type) {
        case FETCH_CATS_REQUESTED:
            return { ...previousState, isLoading: true, error: null, result: null, hasRetrieveOnly: null };
        case FETCH_CATS_FAILED:
            return {
                ...previousState,
                isLoading: false,
                error,
                result: failure,
            };
        case FETCH_CATS_SUCCEEDED:
            return { ...previousState, isLoading: false, error: null, result: success, };
        default:
            return previousState;
    }
};

export default catsReducers;
