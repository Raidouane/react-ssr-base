import { all } from 'redux-saga/effects';

import fetchCatsSaga from './cats/sagas';

export default function* rootSaga() {
    yield all([
        fetchCatsSaga(),
    ]);
}

