import styled from 'react-emotion';
import Particles from 'react-particles-js';

export const Cover = styled('div')({
    background: 'radial-gradient(ellipse at bottom, #1b2735 0%, #090a0f 100%)',
    height: '100%',
});

export const ParticlesStyled = styled(Particles)({
    background: 'radial-gradient(ellipse at bottom, #1b2735 0%, #090a0f 100%)',
    height: '100%',
    display: 'block',
});

export const styles = () => ({
    canvasParticles: {
        display: 'block',
    },
});
