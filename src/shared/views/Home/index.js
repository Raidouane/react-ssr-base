import React from 'react';
import Helmet from 'react-helmet';
import withStyles from '@material-ui/core/styles/withStyles';

import particlesParams from './particles.json';
import { Cover, ParticlesStyled, styles } from './styles';

const Index = ({ classes }: { classes: Object }) => (
    <Cover>
        <Helmet title="Welcome" />
        <ParticlesStyled params={particlesParams} canvasClassName={classes.canvasParticles} />
    </Cover>
);

export default withStyles(styles)(Index);
