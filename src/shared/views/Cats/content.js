// @flow
import React from 'react';
import Helmet from 'react-helmet';

import { CatImg, Cover } from './styles';
import type { FunctionnalComponent} from '../../app/types';

const catView = ({ url }: { url: string }): FunctionnalComponent => (
    <Cover>
        <Helmet title="Cat pictures" />
        {url && <CatImg src={url} alt="cat" />}
    </Cover>
);

export default catView;
