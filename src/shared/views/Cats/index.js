import { connect } from 'react-redux';
import { path } from 'ramda';
import { compose, lifecycle, setPropTypes } from 'recompose';
import { string } from 'prop-types';

import Content from './content';
import { fetchCats } from '../../redux/cats/actions';

const lifecyclePlayload = {
    componentDidMount() {
        this.props.fetchCats();
    },
};

const mapStateToProps = state => ({
    url: path(['cats', 0], state),
});

const connectToStore = connect(
    mapStateToProps,
    { fetchCats },
);

const container = compose(
    connectToStore,
    lifecycle(lifecyclePlayload),
    setPropTypes({
        url: string,
    }),
)(Content);

export default container;
