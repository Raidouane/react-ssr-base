import styled from 'react-emotion';

import ReactImage from '../../Images/react.gif';

export const Cover = styled('div')({
    backgroundImage: `url(${ReactImage})`,
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
});

export const CatImg = styled('img')({
    borderRadius: '50%',
    height: 250,
    width: 250,
});
